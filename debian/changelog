gd4o (1.0~alpha5.git20220521.bb52fb2-2) unstable; urgency=medium

  * Team upload
  * Fix compilation with OCaml 5.2.0 (Closes: #1073880)

 -- Stéphane Glondu <glondu@debian.org>  Wed, 07 Aug 2024 03:27:53 +0200

gd4o (1.0~alpha5.git20220521.bb52fb2-1) unstable; urgency=medium

  * d/watch: fetch current git commits
  * New upstream version 1.0~alpha5.git20220521.bb52fb2
  * Use dh_dune build system
  * Convert copyright file to machine readable format
  * Add Kyle to uploaders (Closes: #934045)

 -- Kyle Robbertze <paddatrapper@debian.org>  Sat, 25 Nov 2023 13:06:12 +0000

gd4o (1.0~alpha5-10) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Team upload
  * Drop transitional packages libgd-gd2-noxpm-* (Closes: #878532)
  * Fix typo in description (Closes: #645001)
  * Add Rules-Requires-Root: no

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.6.2, no changes needed.

 -- Stéphane Glondu <glondu@debian.org>  Sat, 12 Aug 2023 07:47:33 +0200

gd4o (1.0~alpha5-9) unstable; urgency=medium

  * Team upload
  * Switch to dh and bump debhelper compat level to 12
  * Bump Standards-Version to 4.4.0
  * Update Vcs-*
  * Remove Samuel and Romain from Uploaders

 -- Stéphane Glondu <glondu@debian.org>  Tue, 06 Aug 2019 13:54:58 +0200

gd4o (1.0~alpha5-8) unstable; urgency=low

  * Really fix #709853

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 15 May 2013 10:19:47 -0500

gd4o (1.0~alpha5-7) unstable; urgency=low

  * Added BreaksL libgd-gd2-noxpm-ocaml (<< 1.0~alpha5-6~)
    to libgd-ocaml-dev.
  Closes: #709858
  * Added debian/tmp/usr/lib/ocaml to libgd-ocaml.dirs
  Closes: #709853

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 15 May 2013 09:19:41 -0500

gd4o (1.0~alpha5-6) unstable; urgency=low

  * Rebuild against new libgd.
  * Replace xpm/noxpm packages with dummy
    transitional packages.
  * Bump standards version to 3.9.4

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 14 May 2013 19:49:38 -0500

gd4o (1.0~alpha5-5) unstable; urgency=low

  * Team upload
  * Build-Depend on libpng-dev instead of libpng12-dev (Closes: #662342)

 -- Stéphane Glondu <glondu@debian.org>  Mon, 05 Mar 2012 21:28:19 +0100

gd4o (1.0~alpha5-4) unstable; urgency=low

  * Disable native build on archs without ocaml native compiler.

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 12 Oct 2011 02:11:37 +0200

gd4o (1.0~alpha5-3) unstable; urgency=low

  * Also build native library.

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 12 Oct 2011 01:12:02 +0200

gd4o (1.0~alpha5-2) unstable; urgency=low

  * Patched Makefile to pass -fPIC to CFLAGS.
  * Added patch to support flags given through make
    invocation.
  * Added explicit build-deps on libs referenced in Makefile.

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 11 Oct 2011 18:51:33 +0200

gd4o (1.0~alpha5-1) unstable; urgency=low

  * Initial upload to sid (Closes: #644824)

 -- Romain Beauxis <toots@rastageeks.org>  Sun, 09 Oct 2011 15:40:04 +0200
